from docx import Document
from docx.shared import Inches
import datetime

def create_schedule_with_table():
    doc = Document()

    subject_name = input("Введите название предмета: ")
    doc.add_paragraph('Расписание для предмета: ' + subject_name, 'Title')
    schedule_data = {}
    weeks = ["Числитель", "Знаменатель"]
    days = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"]
    short_days = ["ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ"]
    times = ["1-2 8:30", "3-4 10:10", "5-6 11:50", "7-8 13:40", "9-10 15:20", "11-12 17:00", "13-14 18:35", "15-16 20:05"]
    start_date = datetime.date(2024, 2, 9)  # Учеба началась 9 февраля
    end_date = datetime.date(2024, 6, 6)  # Учеба заканчивается 6 июня
    week_day_mapping = {0: "Понедельник", 1: "Вторник", 2: "Среда", 3: "Четверг", 4: "Пятница", 5: "Суббота", 6: "Воскресенье"}

    for week in weeks:
        schedule_data[week] = {day: {time: "" for time in times} for day in days}

    for week in weeks:
        print(f"Расписание на {week}:")
        for day in days:
            print(f"Расписание на {day}:")
            for time in times:
                group_name = input(f"Введите название группы для занятия {subject_name} на {time.split(' ')[1]} (или 'нет' для пропуска): ")
                if group_name.lower() != 'нет':
                    schedule_data[week][day][time] = group_name

    table = doc.add_table(rows=1, cols=len(days)*len(weeks)+1)
    table.style = 'Table Grid'
    hdr_cells = table.rows[0].cells
    hdr_cells[0].text = ''
    for i, week in enumerate(weeks):
        for j, day in enumerate(short_days, start=1):
            hdr_cells[i*len(days)+j].text = day[:2] + f" ({week[0]})"

    for time in times:
        if True:
            row_cells = table.add_row().cells
            paragraph = row_cells[0].paragraphs[0]
            run = paragraph.add_run(f"{time.split(' ')[0]}")
            run.bold = True
            paragraph.add_run(f" {time.split(' ')[1]}")
            for i, week in enumerate(weeks):
                for j, day in enumerate(days, start=1):
                    row_cells[i*len(days)+j].text = f"{schedule_data[week][day][time]}" if schedule_data[week][day][time] else ""
    months = ["Февраль", "Март", "Апрель", "Май", "Июнь"]
    month_mapping = {"Февраль": 2, "Март": 3, "Апрель": 4, "Май": 5, "Июнь": 6}
    current_date = start_date

    for month in months:
        row_cells = table.add_row().cells
        row_cells[0].text = month
        for i in range(1, len(row_cells) // 2 + 1):
            day_of_week = (i - 1) % len(days)
            week_type = weeks[(i - 1) // len(days)]
            dates = []

            temp_date = current_date
            while temp_date.month == month_mapping[month] and temp_date <= end_date:
                if week_day_mapping[temp_date.weekday()] == days[day_of_week] and \
                        (temp_date.isocalendar()[1] - start_date.isocalendar()[1]) % 2 == weeks.index(week_type):
                    dates.append(str(temp_date.day))
                temp_date += datetime.timedelta(days=1)

            row_cells[i + len(row_cells) // 2].text = ", ".join(dates)

        for i in range(len(row_cells) // 2 + 1, len(row_cells)):
            day_of_week = (i - 1) % len(days)
            week_type = weeks[(i - 1) // len(days)]
            dates = []

            temp_date = current_date
            while temp_date.month == month_mapping[month] and temp_date <= end_date:
                if week_day_mapping[temp_date.weekday()] == days[day_of_week] and \
                        (temp_date.isocalendar()[1] - start_date.isocalendar()[1]) % 2 == weeks.index(week_type):
                    dates.append(str(temp_date.day))
                temp_date += datetime.timedelta(days=1)

            row_cells[i - len(row_cells) // 2].text = ", ".join(dates)

        while current_date.month != month_mapping[month] % 12 + 1:
            current_date += datetime.timedelta(days=1)

    image_path = input("Укажите путь к изображению планировщика: ")
    try:
        doc.add_picture(image_path, width=Inches(4))
    except Exception as e:
        print(f"Ошибка при добавлении изображения: {e}")
    output_path = f"расписание_{subject_name}.docx"
    doc.save(output_path)
    print(f"Документ успешно создан: {output_path}")
create_schedule_with_table()

